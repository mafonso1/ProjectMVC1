﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Z_Market.Models
{
    public class Customer
    {
        [Key]
        public int CustomerID { get; set; }


        [StringLength(30, ErrorMessage = "The field {0} must contain between {2} and {1} caracters", MinimumLength = 3)]
        [Required(ErrorMessage = "You must enter the field {0}")]
        [Display(Name = " First Name")]


        public string ContactFirstName { get; set; }

        [StringLength(30, ErrorMessage = "The field {0} must contain between {2} and {1} caracters", MinimumLength = 3)]
        [Required(ErrorMessage = "You must enter the field {0}")]
        [Display(Name= " Last Name")]

        public string ContactLastName { get; set; }

        [StringLength(30, ErrorMessage = "The field {0} must contain between {2} and {1} caracters", MinimumLength = 3)]
        [Required(ErrorMessage = "You must enter the field {0}")]

        public string Phone { get; set; }
        [StringLength(30, ErrorMessage = "The field {0} must contain between {2} and {1} caracters", MinimumLength = 3)]
        [Required(ErrorMessage = "You must enter the field {0}")]

      public string Address { get; set; }

        [StringLength(30, ErrorMessage = "The field {0} must contain between {2} and {1} caracters", MinimumLength = 3)]
        [Required(ErrorMessage = "You must enter the field {0}")]
        public string Email { get; set; }

        [StringLength(20, ErrorMessage = "The field {0} must contain between {2} and {1} caracters", MinimumLength = 5)]
        [Required(ErrorMessage = "You must enter the field {0}")]
    
        public string Document { get; set; }


        // se coloca el ? para indicar que el campo es opcional.
        public int? DocumentTypeID { get; set; }

        //Lado uno de la relacion...
        public virtual DocumentType DocumentType { get;set; }

        //lado uno de la relacion Order - Customer
        public virtual ICollection<Order> Orders { get; set; }




    }
}
