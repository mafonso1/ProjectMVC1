﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Xml.Linq;

namespace Z_Market.Models
{
    public class DocumentType
    {
        [Key]
        [Display(Name = "Document Type")]
        public int DocumentTypeID { get; set; }
        [Display(Name = "Document")]
        public string Description { get; set; }


        // 1 tipo de documento tiene varios empleados.....
        public virtual ICollection<Employee> Employess { get; set; }
        public virtual ICollection<Customer> Customers { get; set; }

    }
}