﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Z_Market.Models
{
    public class Order
    {
        [Key]
        public int OrderID { get; set; }

        public DateTime DayOrder { get; set; }
        public int CustomerID { get; set; }
        public OrderStatus OrderStatus { get; set; }

        //Lado varios de la relacion Order-Customer
        public virtual Customer Customer { get; set; }
        public virtual ICollection<OrderDetails> OrderDetails { get; set; }


    }
}