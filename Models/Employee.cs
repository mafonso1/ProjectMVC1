﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Z_Market.Models
{
    public class Employee
    {
        // Sirve para renombrar el nombre de la tabla en base de datos 
        //[Table("Empleados")]
        [Key]
        public int EmployeeID { get; set; }
        //Cambia el nombre del campo en base de datos.
        [Column("Name")]
        // Para Personalizar el Campo
        [Display(Name = "Firts Name")]
        //Para que sea obligatorio ingresar este dato
        [Required(ErrorMessage = "You must enter {0}")]
        [StringLength(30, ErrorMessage = "The field {0} must be between {1} and {2} caracter", MinimumLength = 3)]
        public string FirstName { get; set; }


        [Display(Name = "Last Name")]
        [Required(ErrorMessage = "You must enter {0}")]
        [StringLength(30, ErrorMessage = "The field {0} must be between {1} and {2} caracter", MinimumLength = 3)]
        public string LastName { get; set; }


        [Required(ErrorMessage = "You must enter {0}")]
        // Para editar el formato de la moneda
        [DisplayFormat(DataFormatString = "{0:C2}", ApplyFormatInEditMode = false)]
        public decimal Salary { get; set; }


        [Display(Name = "Bonus Percent")]
        [DisplayFormat(DataFormatString = "{0:P2}", ApplyFormatInEditMode = false)]
        public float BonusPercent { get; set; }


        [Display(Name = "Date Of Birth")]
        [Required(ErrorMessage = "You must enter {0}")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        public DateTime DateOfBith { get; set; }


        [Display(Name = "Star Time")]
        [Required(ErrorMessage = "You must enter {0}")]
        [DataType(DataType.Time)]
        [DisplayFormat(DataFormatString = "{0:hh:mm}", ApplyFormatInEditMode = true)]

        public DateTime StarTime { get; set; }
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }
        [DataType(DataType.Url)]
        public string Url { get; set; }

        [Display (Name ="Document")]
        public int DocumentTypeID { get; set; }

        //Para que no se agregue a la abase de datos.
        [NotMapped]
        //para calcular la edad..
        public int Age { get { return DateTime.Now.Year - DateOfBith.Year; } }
        
        //se vincula del lado Varios ....
        public virtual DocumentType DocumentType { get; set; }





    }
}