﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Z_Market.Models
{
    public class Product
    {
        [Key]
        public int ProductID { get; set; }
        [StringLength(30,ErrorMessage ="The flied {0} must contain between {2} and {1} caracters",MinimumLength = 3)]
        [Required(ErrorMessage ="Yoy must enter the field {0}")]
        [Display(Name = "Product Description")]

        public string Description { get; set; }
        [DataType(DataType.Currency)]
        [DisplayFormat(DataFormatString = "{0:C2}", ApplyFormatInEditMode = false)]
        [Required(ErrorMessage = "Yoy must enter the field {0}")]
        public decimal Price { get; set; }

        [DataType(DataType.Date)]
        [Display(Name ="Last Buy")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime LastBuy { get; set; }

        [DataType(DataType.Currency)]
        [DisplayFormat(DataFormatString = "{0:N2",ApplyFormatInEditMode =  false)]
        public float Stock { get; set; }


        [DataType(DataType.MultilineText)]
        public string Remarks { get; set; }

        public virtual ICollection<SupplierProduct> SupplierProducts { get; set; }
        public virtual ICollection<OrderDetails> OrderDetails { get; set; }


    }
}